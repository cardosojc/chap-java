package org.ulht.meisi.sgi;

import java.io.IOException;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.logging.Logger;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

@ServerEndpoint("/server")
public class ChapServer {

	private static final String PASSWORD_INCORRECTA = "PASSWORD_INCORRECTA";

	private static final String SUCESSO = "SUCESSO";

	private static final String INEXISTENT_USER = "INEXISTENT_USER";

	private final Logger logger = Logger.getLogger(this.getClass().getName());
	
	private MongoClient mongoClient;
	private DB db;
	private StringBuilder nonce = new StringBuilder();
	private DBCollection coll;
	private String user;

	@OnOpen
	public void onConnectionOpen(Session session) {
		logger.info("Connection opened ... " + session.getId());
		try {
			mongoClient = new MongoClient("localhost",27017);
			db = mongoClient.getDB("meisi");
			coll = db.getCollection("users");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	@OnMessage
	public String onMessage(String message) {		
		String result = null;
		String[] fields = message.split(":");
		if(fields[0].equals("username")){			
			BasicDBObject query = createQuery("username", fields[1]);
			Cursor cursor = coll.find(query);
			if(cursor.hasNext()){
				nonce.append(System.currentTimeMillis());
				Random random = new Random();
				nonce.append(random.nextInt());
				result = nonce.toString();
				user = fields[1];
			}else{
				result = INEXISTENT_USER;
			}
		}else if(fields[0].equals("password")){
			BasicDBObject query = createQuery("username", user);
			Cursor cursor = coll.find(query);
			String password = cursor.next().get("password").toString();
			byte[] finalDigest = null;
			StringBuilder sb = new StringBuilder();
			try {
				sb.append(password);
				sb.append(nonce);
		        MessageDigest md = MessageDigest.getInstance("MD5");
		        md.update(sb.toString().getBytes());
				finalDigest = md.digest();				
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			if(getMD5String(finalDigest).equalsIgnoreCase(fields[1])){
				result = SUCESSO;
			}else{
				result = PASSWORD_INCORRECTA;
			}
		}
		return result;
	}
	
	private String getMD5String(byte[] bytes){
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
         sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
	}
	
	private BasicDBObject createQuery(String parameter, String value){
		return new BasicDBObject(parameter, value);
	}

	@OnClose
	public void onConnectionClose(Session session) {
		logger.info("Connection close .... " + session.getId());
		try {
			session.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public MongoClient getMongoClient() {
		return mongoClient;
	}

	public void setMongoClient(MongoClient mongoClient) {
		this.mongoClient = mongoClient;
	}

	public DB getDb() {
		return db;
	}

	public void setDb(DB db) {
		this.db = db;
	}

	public StringBuilder getNonce() {
		return nonce;
	}

	public void setNonce(StringBuilder nonce) {
		this.nonce = nonce;
	}

	public DBCollection getColl() {
		return coll;
	}

	public void setColl(DBCollection coll) {
		this.coll = coll;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
