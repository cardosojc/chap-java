var wsUrl;
if (window.location.protocol == 'http:') {
  	wsUrl = 'ws://' + window.location.host + '/chap/server';
} else {
  	wsUrl = 'wss://' + window.location.host + '/chap/server';
}
console.log('WebSockets Url : ' + wsUrl);
var ws = new WebSocket(wsUrl);
var nonce;
var user;

ws.onopen = function(event){
	console.log('WebSocket connection started');
};

ws.onclose = function(event){
	 console.log("Remote host closed or refused WebSocket connection");
     console.log(event);
};

ws.onmessage = function(event){
    nonce = event.data;
    if(nonce == 'INEXISTENT_USER'){
    	alert('Este utilizador não existe!!');
    	location.reload();
    }else if(nonce == 'SUCESSO'){
    	alert('Autenticação efectuada com sucesso!!');
    	$('#divUser').remove();
    	$('#divPwd').remove();
    	$("#divAuth").text('Bem Vindo, '+user+". "+ new Date());
    	$("#divAuth").removeClass("hidden");	
    }else if(nonce == 'PASSWORD_INCORRECTA'){
    	alert('Password Incorrecta!!');
        location.reload();
    }else{
    	$("#divPwd").removeClass("hidden");	
    }
};

$("button#usernameSubmit").on('click',function(){
    var message = $('textarea#username').val();
    ws.send('username:'+message);
    user = message;
});

$("button#passwordSubmit").on('click',function(){
    var password = $('#password').val();
    var pass = CryptoJS.MD5(CryptoJS.MD5(password)+nonce);
    ws.send('password:'+pass);
});

